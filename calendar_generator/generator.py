# -*- coding: utf-8 -*-
"""
calendar generator
"""
import os
import logging
import calendar
from datetime import date
from typing import List, Optional, Mapping
from weasyprint import HTML


from jinja2 import Environment, select_autoescape, FileSystemLoader, exceptions
import weasyprint

logger = logging.getLogger(__name__)
DEFAULT_FIRST_WEEK_DAY = 1


class CalendarGenerator:
    """
    Generate a calendar page from a template
    """

    month = None
    year = None
    image = None
    images = None

    template_name: str = "base.html"
    _calendar = None

    def get_calendar(self):
        return self._calendar

    def set_calendar(self, val):
        self._calendar = val

    calendar = property(get_calendar, set_calendar, None, "Gets / Sets calendar")

    @property
    def weekdays(self) -> list:
        return ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"]

    def __init__(self, *args, **kwargs):
        _today = date.today()
        self.month = int(kwargs.pop("month", "0")) or _today.month
        self.year = int(kwargs.pop("year", "0")) or _today.year
        self.image = (
            kwargs.pop("image", None) or "file:///home/frague/Images/frague.jpg"
        )
        self.images = kwargs.pop("images", None)

        self._cal = calendar.Calendar()
        self._cal.firstweekday = DEFAULT_FIRST_WEEK_DAY

        assert self.year is not None, "Year cannot be None"

        if self.month is None:
            self.calendar = self._cal.yeardatescalendar(self.year)
        else:
            self.calendar = self._cal.monthdatescalendar(self.year, self.month)

    def get_context(self) -> Mapping:
        """
        Gets the context to be rendered
        """
        output = {
            "title": "Hello, world !",
            "calendar": self.calendar,
            "image": self.image,
            "month": self.month,
            "year": self.year,
            "weekdays": self.weekdays,
        }
        return output

    def get_template_name(self) -> str:
        """
        Gets the template name
        """
        return self.template_name or "base.html"

    def _to_html(self):
        """
        Renders the calendar page according to template
        """
        loader = FileSystemLoader(os.path.join("calendar_generator", "templates"))
        env = Environment(loader=loader, autoescape=select_autoescape(["html", "xml"]))
        template_name = self.get_template_name()
        try:
            template = env.get_template(template_name)
        except exceptions.TemplateNotFound as tnf:
            logger.fatal("Template not found: %s", tnf)
        html = template.render(**self.get_context())
        return html

    def _to_pdf(self, html: str, filename: str) -> None:
        _html = HTML(string=html)
        _bytes = _html.write_pdf(filename)
    
    def render(self):
        html = self._to_html()
        pdf = self._to_pdf(html, "/tmp/test.pdf")
        return pdf
