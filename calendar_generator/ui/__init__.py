# -*- coding: utf-8 -*-
import logging
from typing import Optional
from tkinter import *
from copy import deepcopy
from tkinter.ttk import *
from gettext import gettext as _
from typing import List, Union
from .styles import STYLE

logger = logging.getLogger(__name__)


class UiWindow(Tk):
    tk_title = _("Calendar generator")
    tk_iconname = "calendar"
    tk_minsize = (640, 480)

    def __init__(
        self, 
        month: int, 
        year: int, 
        verbose: bool = False,
        debug: bool = False,
        image: Optional[str] = None
    ):
        # Gets the kwargs
        self.month = month
        self.year = year
        self.verbose = verbose
        self.debug = debug
        self.image = image
       
        super(UiWindow, self).__init__(
            # title=self.title, 
            # iconname=self.tk_iconname
        )
        self.geometry("640x480+300+300")
        # self.minsize(self.tk_minsize)

        # Initialize the window
        self.minsize = kwargs.pop("minsize", self.minsize)
        self.frame = PanedWindow(self, orient=HORIZONTAL)
        self.frame.pack()
        self._add_dates_frame()
        self._add_close_button()

    def _add_dates_frame(self):
        self.dates_frame = Frame(self.frame, style="BW.TLabel")
        self.dates_frame.pack()

        self.date_list_box = Listbox(self.dates_frame)
        self.date_list_box.pack()

    def _add_close_button(self): 
        # Adds a close button
        self.close_button = Button(
            self, text=_("Close"), command=self.quit(), style="BW.TButton"
        )
        self.close_button.pack()

    def _action_load_image(self):
        ...

    def _add_to_dates_list_box(self, value: str):
        self.date_list_box.append(value)
        self.date_list_box.pack()
        return self.date_list_box


class UiFrame(Frame):
    def __init__(self, master=None, **kwargs):
        self.master = master
        _tk_kwargs = deepcopy(kwargs)
        super().__init__(master=master, **_tk_kwargs)

