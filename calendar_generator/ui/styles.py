# -*- coding: utf-8 -*-
"""
Styling for Tk widgets 
"""
from tkinter.ttk import Style

light_style = Style()
light_style.configure("BW.TLabel", foreground="black", background="grey")
light_style.configure("BW.TButton", foreground="black", background="grey")

dark_style = Style()
dark_style.configure("BW.TLabel", foreground="white", background="darkgrey")
dark_style.configure("BW.TButton", foreground="white", background="darkgrey")

STYLE = dark_style