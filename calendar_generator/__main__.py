#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from typing import Optional 
from datetime import date
import logging
from calendar_generator.ui import UiWindow
from calendar_generator.generator import CalendarGenerator
import click


@click.command()
@click.option(
    "-c", 
    "--cli", 
    default=True, 
    required=False, 
    is_flag=True, 
    help="Starts in CLI mode"
)
@click.option(
    "-g",
    "--gui",
    default=False,
    required=False,
    is_flag=True,
    help="Starts in GUI mode",
)
@click.option(
    "-v",
    "--verbose",
    default=False,
    required=False,
    is_flag=True,
    help="Displays verbose messages",
)
@click.option("-d", "--debug", default=False, required=False, is_flag=True, help="Displays DEBUG messages")
def main(
    cli: bool, 
    gui: bool, 
    verbose: bool, 
    debug: bool, 
    year: Optional[int] = None, 
    month: Optional[int] = None
) -> int:
    """
    Main entry 
    """
    if year is None:
        year = date.today().year
    if month is None:
        month = date.today().month

    if gui:
        return _main_ui(year=year, month=month, debug=debug, verbose=verbose)
    else:
        return _main_cli(year=year, month=month, debug=debug, verbose=verbose)


def _main_ui(**kwargs):
    """
    Starts the UI 
    """
    window = UiWindow(**kwargs)
    window.mainloop()
    return 0


def _main_cli(**kwargs):
    """
    Starts the CLI 
    """
    generator = CalendarGenerator(**kwargs)
    _bytes = generator.render()
    # sys.stdout.write(_bytes)
    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
